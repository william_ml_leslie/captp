from functools import wraps
from api import CapTPException

def eject(ej, error):
    if ej:
        ej(error)
    raise error

def fail(sp, g, ej, msg=None):
    eject(ej, CapTPException(("Not accepted by %(g)s" +
                              (": %(msg)s" if msg else ""))
                             % dict(g=g, msg=msg)))

def _ncmp(x, y, n):
    if x is None:
        return y
    elif y is None:
        return x
    return n(x, y)


class ExactTypeGuard(object):
    def __init__(self, ty):
        self.type = ty

    def __str__(self):
        return "T.%sT" % (self.type.__name__,)

    def coerce(self, sp, ej=None):
        if type(sp) is self.type:
            return sp
        else:
            fail(sp, self, ej)


class CtorTypeGuard(ExactTypeGuard):
    def coerce(self, sp, ej=None):
        try:
            sp = self.type(sp)
        except Exception:
            fail(sp, self, ej)
        else:
            return super(CtorTypeGuard, self).coerce(sp, ej)


class LongTypeGuard(object):
    def __str__(self):
        return "T.intT"

    def coerce(self, sp, ej):
        try:
            return int(sp)
        except Exception:
            fail(sp, self, ej)




class BooleanTypeGuard(object):
    boolean_false_strings = frozenset(["False", "false", "0"])
    boolean_true_strings = frozenset(["True", "true", "1"])

    def __str__(self):
        return "T.booleanT"

    def coerce(self, sp, ej=None):
        try:
            if sp in self.boolean_false_strings:
                return False
            if sp in self.boolean_true_strings:
                return True
            fail(sp, self, ej)
        except Exception:
            fail(sp, self, ej)


class AnyTypeGuard(object):
    def __str__(self):
        return "T.AnyT"

    def coerce(self, sp, ej=None):
        return sp


class BoundedGuard(object):
    def __init__(self, base_type, least=None, most=None):
        self.base_type = base_type
        self.least = least
        self.most = most

    def __str__(self):
        result = str(self.base_type)
        if self.least is not None:
            result += ".at_least(%d)" % (self.least,)
        if self.most is not None:
            result += ".at_most(%d)" % (self.most,)
        return result

    def coerce(self, sp, ej=None):
        sp = self.base_type.coerce(sp, ej)
        if self.least is not None and sp < self.least:
            fail(sp, self, ej)
        if self.most is not None and sp > self.most:
            fail(sp, self, ej)
        return sp

    def at_least(self, n):
        return BoundedGuard(self.base_type, _ncmp(self.least, n, max),
                            self.most)

    def at_most(self, n):
        return BoundedGuard(self.base_type, self.least,
                            _ncmp(self.most, n, min))

    def above(self, n):
        return self.at_least(n + 1)

    def below(self, n):
        return self.at_most(n - 1)

intT = BoundedGuard(LongTypeGuard())
booleanT = BooleanTypeGuard()
functionT = ExactTypeGuard(type(eject))
numberT = CtorTypeGuard(float)
#objectT = ExactTypeGuard()
stringT = ExactTypeGuard(unicode)
noneT = ExactTypeGuard(type(None))
plain_arrayT = ExactTypeGuard(list)
anyT = AnyTypeGuard()

incoming_posT = intT.above(0)
answer_posT = intT.below(0)
import_posT = intT.at_least(0)
wire_deltaT = intT.below(2 ** 8)
msg_nameT = stringT
nonceT = intT

def guard(*types):
    def decorate(f):
        @wraps(f)
        def wrapped(self, *args):
            coerced_args = [guard.coerce(val)
                            for guard, val in zip(types, args)]
            return f(self, *coerced_args)
        return wrapped
    return decorate
