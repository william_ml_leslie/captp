import zope.interface as zi
from captp import api, datae, uncallers

@zi.implementer(api.IUncallable)
class SelflessUncallable(object):
    def __init__(self, target):
        self.target = target

    def opt_uncall(self, builder, captp):
        model, name = self.target.get_model()
        data = api.get_data(captp, self.target)
        return (model, name, data)


@zi.implementer(api.IUncallable)
class SelfishUncallable(object):
    def __init__(self, target):
        self.target = target

    def opt_uncall(self, builder, captp):
        model, name = self.target.get_model()
        args = [uncallers.ByRefUncaller(self)]
        args.extend(api.get_data(captp, self.target))
        return (model, name, args)


def add_model_getter(model, cls):
    name = cls.__name__.decode('utf-8')
    def get_model(self):
        return model, name
    cls.get_model = get_model


@zi.implementer(api.IRepresentable)
class Model(object):
    def __init__(self, name):
        self.name = name

    def represent(self, recogniser):
        return datae.Import(self.name)

    def selfish(self, cls):
        add_model_getter(self, cls)
        return zi.implementer(api.IHaveModel, api.ISelfish,
                              api.IHasPublicData)(cls)

    def selfless(self, cls):
        add_model_getter(self, cls)
        return zi.implementer(api.IHaveModel, api.ISelfless,
                              api.IHasPublicData)(cls)
