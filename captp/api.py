import zope.interface as zi

WHEN_MORE_RESOLVED_MESSAGE = "CapTP_whenMoreResolved"
WHEN_BROKEN_MESSAGE = "CapTP_whenBroken"


class IRepresentable(zi.Interface):
    def represent(self, recogniser):
        pass


class IUncallable(zi.Interface):
    """An object that supports custom datae serialisation
    """
    def opt_uncall(self, builder, captp):
        """Serialise obj using the builder
        """
        pass


class IRef(zi.Interface):
    """An object that can receive messages.

    This method may be called synchronously.
    """
    def on_message(self, verb, args):
        pass


class CapTPException(Exception):
    pass


def adapt(registry, interface, value):
    adapted = registry.queryAdapter(value, interface)
    if adapted:
        return adapted
    return interface(value, None)

def get_data(captp, value):
    data_getter = adapt(captp.uncallers, IHasPublicData, value)
    if not data_getter:
        data_getter = IHasPublicData(value, None)
    if not data_getter:
        return []
    return data_getter.get_data()


def opt_uncall(captp, ob):
    # first, attempt a type-specific adapter.
    adapted = captp.uncallers.queryAdapter(ob, IUncallable)
#    adapted = captp.uncallers.queryAdapter(
#        zi.implementedBy(type(ob)), IUncallable)
#    if not adapted:
#        print "Not adapted: %r : %r" % (ob, list(zi.implementedBy(type(ob))))
#        adapted = captp.uncallers.queryAdapter([type(ob)], IUncallable)
    if not adapted:
        adapted = IUncallable(ob)
    return adapted.opt_uncall(None, captp)


class IHaveIdentity(zi.Interface):
    def get_identity(self):
        pass


# @zi.implementer(IUncallable)
# class ISelfIdentity(object):
#     def __init__(self, value):
#         self.value

#     def get_identity(self):
#         return self.value


class IHasPublicData(zi.Interface):
    def get_data(self):
        pass


class ISelfless(zi.Interface):
    pass

class ISelfish(zi.Interface):
    pass

class IHaveModel(zi.Interface):
    pass
