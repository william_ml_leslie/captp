import unittest
import zope.interface as zi
from captp import datae as jt, model_support, api
from captp.model_support import Model
from captp.datae import serialise
from twisted.internet import defer
import captp.uncallers

class ByID(object):
    def __init__(self, value):
        self.value = value

    def __hash__(self):
        return id(self.value)

    def __eq__(self, other):
        if isinstance(other, ByID):
            return self.value is other.value
        return self.value is other

class BogusSwissTable(object):
    def next(self):
        return BOGUS_SWISS

    def get_new(self, identity):
        return BogusSwissNumber()

class BogusSwissNumber(object):
    def hash(self):
        return BOGUS_SWISS_HASH

FAR_POS = 1729
QUESTION = 3
EXPORT = 7
BOGUS_SWISS = u"snth"
BOGUS_SWISS_HASH = u"sanotehu"

@zi.implementer(api.IRepresentable)
class DescMaker(object):
    def represent(self, recogniser):
        return recogniser.builder.build_import(u'CapTP_1_descs')


class SimpleTestCapTP(object):
    desc_maker_standin = DescMaker()
    uncallers = captp.uncallers.create_registry()
    swiss_table = BogusSwissTable()

    def lookup(self, value):
        return None

    def bind_export(self, ob):
        return EXPORT

    def prepare_question(self, something):
        return QUESTION, defer.Deferred()

    def index_for(self, identity):
        return None

    def new_far_pos(self, identity):
        return FAR_POS


class TestSerialisation(unittest.TestCase):
    def test_list(self):
        captp = SimpleTestCapTP()
        self.assertEqual(serialise([0, 1.5, u"hello"], captp),
                         [u"call", [u"import", u"CapTP_1_descs"],
                          "List", [0, 1.5, u"hello"]])

    def test_dict(self):
        captp = SimpleTestCapTP()
        self.assertEqual(serialise({u'foo' : u'bar', u'baz' : 7}, captp),
                         [u"call", [u"import", u"CapTP_1_descs"],
                          u"Dict", [u'baz', 7, u'foo', u'bar']])

    def test_ob_selfish(self):
        model = Model(u"Model1")
        @model.selfish
        class Selfish1(object):
            def get_data(self):
                return [u"some", u"data"]

        captp = SimpleTestCapTP()
        # selfish models get passed a ref as their first argument.
        # performs expression:
        #   Model1.Selfish1(CapTP_1_descs.NewFar(0, ""), "some", "data")
        self.assertEqual(serialise(Selfish1(), captp),
                         [u"call", [u"import", u"Model1"],
                          u"Selfish1", [[u"call",
                                         [u"import", u"CapTP_1_descs"],
                                         u"NewFar",
                                         [FAR_POS, BOGUS_SWISS_HASH]],
                                       u"some", u"data"]])

    def test_ob_selfless(self):
        model = Model(u"Model2")
        @model.selfless
        class Selfless1(object):
            def get_data(self):
                return [u"some", u"data"]

        captp = SimpleTestCapTP()
        # performs expression:
        #   Model2.Selfless1("some", "data")
        self.assertEqual(serialise(Selfless1(), captp),
                         [u"call", [u"import", u"Model2"],
                          u"Selfless1", [u"some", u"data"]])

    def test_promise(self):
        d = defer.Deferred()
        captp = SimpleTestCapTP()
        self.assertEqual(serialise(d, captp),
                         [u"call", [u"import", u"CapTP_1_descs"],
                          u"NewRemotePromise",
                          [EXPORT, QUESTION, BOGUS_SWISS]])

    def test_recursion(self):
        x = []
        x.append(x)
        captp = SimpleTestCapTP()
        self.assertEqual(serialise(x, captp),
                         [u'defrec', 0,
                          [u"call", [u"import", u"CapTP_1_descs"],
                           u"List", [[u'ibid', 0]]]])

if __name__ == '__main__':
    unittest.main()
