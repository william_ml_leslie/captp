import unittest
from captp import guard as T, api

class WithGuardedMethods(object):
    @T.guard(T.intT, T.plain_arrayT)
    def foo(self, n, xs):
        return (n, xs)

class TestGuard(unittest.TestCase):
    def test_intT(self):
        intT = T.intT
        self.assertEqual(intT.coerce(7), 7)
        self.assertEqual(intT.coerce(7.0), 7)
        self.assertEqual(intT.coerce("0"), 0)
        self.assertEqual(intT.coerce("1260"), 1260)
        self.assertEqual(intT.coerce("-7"), -7)
        self.assertEqual(intT.coerce("123456781234567812345678"),
                         123456781234567812345678)
        self.assertEqual(intT.coerce("0"), 0)
        self.assertEqual(intT.coerce("0080"), 80)
        self.assertEqual(intT.coerce("-0030"), -30)
        with self.assertRaises(api.CapTPException):
            intT.coerce("0xf7")
        with self.assertRaises(api.CapTPException):
            intT.coerce("0o05")
        with self.assertRaises(api.CapTPException):
            intT.coerce("0b10")
        with self.assertRaises(api.CapTPException):
            intT.coerce("0b10")
        with self.assertRaises(api.CapTPException):
            intT.coerce("not an int")

    def test_intT_bound(self):
        intT = T.intT
        self.assertEqual(intT.below(10).coerce(3), 3)
        self.assertEqual(intT.below(10).coerce(9), 9)
        self.assertEqual(intT.above(10).coerce(11), 11)
        with self.assertRaises(api.CapTPException):
            intT.below(10).coerce(10)
        with self.assertRaises(api.CapTPException):
            intT.below(10).coerce(17)
        with self.assertRaises(api.CapTPException):
            intT.above(10).coerce(10)
        with self.assertRaises(api.CapTPException):
            intT.above(10).coerce(7)
        self.assertEqual(intT.below(10).above(-5).coerce(4), 4)
        self.assertEqual(intT.below(10).above(-5).coerce(-4), -4)
        with self.assertRaises(api.CapTPException):
            intT.below(10).above(-5).coerce(-5)
        with self.assertRaises(api.CapTPException):
            intT.below(10).above(-5).coerce(12)

    def test_booleanT(self):
        self.assertEqual(T.booleanT.coerce("False"), False)
        self.assertEqual(T.booleanT.coerce("True"), True)
        self.assertEqual(T.booleanT.coerce("0"), False)
        self.assertEqual(T.booleanT.coerce("1"), True)
        with self.assertRaises(api.CapTPException):
            T.booleanT.coerce("No")

    def test_numberT(self):
        self.assertEqual(T.numberT.coerce("12.25"), 12.25)
        with self.assertRaises(api.CapTPException):
            T.numberT.coerce("127.0.0.1")

    def test_stringT(self):
        self.assertEqual(T.stringT.coerce(u"sntahoesuntho"),
                         u"sntahoesuntho")

        with self.assertRaises(api.CapTPException):
            # raw bytes
            T.stringT.coerce("asntoheu")
        with self.assertRaises(api.CapTPException):
            T.stringT.coerce(7)

    def test_noneT(self):
        self.assertEqual(T.noneT.coerce(None), None)
        with self.assertRaises(api.CapTPException):
            T.noneT.coerce("Null")
        with self.assertRaises(api.CapTPException):
            T.noneT.coerce(0)
        with self.assertRaises(api.CapTPException):
            T.noneT.coerce(u"whatever")

    def test_plain_arrayT(self):
        self.assertEqual(T.plain_arrayT.coerce([0, 1, "hello"]),
                         [0, 1, "hello"])
        with self.assertRaises(api.CapTPException):
            # generator
            T.plain_arrayT.coerce(x for x in (1, 2))
        with self.assertRaises(api.CapTPException):
            # dict
            T.plain_arrayT.coerce(dict(k="v"))
        with self.assertRaises(api.CapTPException):
            # text
            T.plain_arrayT.coerce(u"snoatehuoae")

    def test_anyT(self):
        class Foo(object):
            pass
        f = Foo()
        self.assertEqual(T.anyT.coerce(f), f)

    def test_guard(self):
        w = WithGuardedMethods()
        self.assertEqual(w.foo("7", [1, 3]), (7, [1, 3]))
        with self.assertRaises(api.CapTPException):
            w.foo("0.7", [])
        with self.assertRaises(api.CapTPException):
            w.foo(7, u"not a list")


if __name__ == '__main__':
    unittest.main()
