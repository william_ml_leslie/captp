from twisted.trial import unittest
from captp import ref, api


class FakeCapTP(object):
    def send_deliver(self, target, message, args):
        self.target = target
        self.message = message
        self.args = args
        return ref.FarRef(self)


class LocalOb(object):
    def on_message(self, verb, args):
        return (verb, args)


class TestRef(unittest.TestCase):
    def test_broken(self):
        exception = api.CapTPException("It didn't work")
        source = ref.Ref.broken(exception)
        self.assertEquals(source.status, ref.BROKEN)
        self.assertEquals(source.problem, exception)
        def on_broken(broken_ref):
            self.assertEquals(source, broken_ref)
            self.assertEquals(broken_ref.status, ref.BROKEN)
            return 7
        def the_rest(number):
            self.assertEquals(number.status, ref.FULFILLED)
            self.assertEquals(number.value, 7)
        return ref.Ref.whenBroken(source, on_broken
                                  ).result.addCallback(the_rest)

    def test_local(self):
        source = LocalOb()
        local = ref.Ref.resolve(source)
        result = ref.E.send(local, "foo", [5, 9])
#        result = local.on_message("foo", [5, 9])
        self.assertEquals(local.status, ref.FULFILLED)
        self.assertEquals(result.status, ref.FULFILLED)
        def test_resolved(resolved):
            self.assertEquals(resolved.value, ("foo", [5, 9]))
        def wait(_):
            return ref.Ref.whenResolved(result, test_resolved).result
        return local.result.addCallback(wait)

    def test_never(self):
        never = ref.NEVER
        self.assertEquals(never.status, ref.UNRESOLVED)
        self.assertEquals(never.on_message("foo", []),
                          never)

    def test_far_ref(self):
        captp = FakeCapTP()
        far = ref.FarRef(captp)
        result = far.on_message("foo", [7])
        self.assertEquals(result.status, ref.UNRESOLVED)
        self.assertEquals(far.status, ref.UNRESOLVED)
        self.assertEquals(captp.target, far)
        self.assertEquals(captp.message, "foo")
        self.assertEquals(captp.args, [7])

    def test_waiting(self):
        resolver, promise = ref.Ref.promise()
        self.assertEquals(promise.status, ref.UNRESOLVED)
        resolver.resolve("hello")
        def test_resolved(resolved):
            self.assertEquals(resolved, ref.Ref.fulfillment(promise))
            self.assertEquals(resolved.status, ref.FULFILLED)
            self.assertEquals(resolved.value, "hello")
        return ref.Ref.whenResolved(promise, test_resolved).result

