"""Common implementations of IRef.

IRefs are promises that also handle CapTP messages with their
on_message method.  They may either be FULFILLED, BROKEN or
UNRESOLVED.  Semantically, they can also be Near or Far.

Refs to local objects can be constructed with the captp.ref.Ref
object.
"""

from twisted.internet.defer import Deferred
import zope.interface as zi

from captp import api


class Sentinel(object):
    __slots__ = ('name',)
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name

FULFILLED = Sentinel("fulfilled")
BROKEN = Sentinel("broken")
UNRESOLVED = Sentinel("unresolved")

UNRESOLVED_SENTINEL = Sentinel("unresolved_sentinel")


def standard_attribute_error(ob, name):
    """Emulate the standard python AttributeError.
    """
    return AttributeError("%r object has no attribute %r" %
                          (type(ob).__name__, name))


def ref_resolution(cls, ref):
    return ref.resolved_handler().addErrback(cls.broken)


def if_smashed(reactor):
    """A callback that will run the reactor if its target is broken.
    """
    def run_if_smashed(ref):
        f = Ref.resolution(ref)
        if f.status is BROKEN:
            return Ref.resolve(reactor(f))
        return NEVER
    return run_if_smashed


class E(object):
    @classmethod
    def call(cls, recipient, verb, args):
        return recipient.on_message(verb, args)

    @classmethod
    def send(cls, recipient, verb, args):
        d = Deferred()
        @d.addCallback
        def callback(r):
            return r.on_message(verb, args)
        d.addCallback(Ref.resolve)
        d.callback(recipient)
        return WaitingRef(d)

    @classmethod
    def sendOnly(cls, recipient, verb, args):
        d.addCallback(Ref.resolve)
        d.addCallback(lambda rec: rec.on_message(name, args))
        d.callback(recipient)

    @classmethod
    def callWithPair(cls, recipient, message):
        return cls.call(recipient, message[0], message[1])

    @classmethod
    def toString(cls, obj):
        return str(obj)

    @classmethod
    def toQuote(cls, obj):
        return repr(obj)


class Ref(object):
    """Public Ref API.

    http://wiki.erights.org/wiki/Object_Ref

    Constructs and queries IRefs.
    """

    @classmethod
    def resolution(cls, ref):
        next_ref = ref.resolution
        while next_ref != ref:
            ref = next_ref
            next_ref = ref.resolution
        return next_ref

    @classmethod
    def fulfillment(cls, ref):
        """Get the resolved form of @ref.

        Raises an exception if not resolved.
        """
        ref = cls.resolution(ref)
        if ref.status is FULFILLED:
            return ref
        if ref.status is BROKEN:
            raise ref.problem
        raise api.CapTPException("Unresolved promise")

    @classmethod
    def resolve(cls, value):
        """Construct a ref.

        If the value is a ref, then it is immediately returned.

        If the value is a promise, a ref representing it is returned.

        Otherwise, return a ref representing the value provided.
        """
        if isinstance(value, Deferred):
            return WaitingRef(value.addErrback(cls.broken))
        if api.IRef.providedBy(value):
            return cls.resolution(value)
        return Fulfilled(value)

    @classmethod
    def promise(cls):
        """Construct a resolver/promise pair.
        """
        d = Deferred()
        return WaitingResolver(d), WaitingRef(d)

    @classmethod
    def broken(cls, problem):
        """Return a promise representing the problem.

        TODO: is it possible for the problem to be a ref?
        """
        return Broken(problem)

    @classmethod
    def whenResolved(cls, ref, reactor):
        """Add a callback @reactor to be run when the ref is resolved.

        Return an IRef representing the result of the reactor.
        """
        return WaitingRef(ref_resolution(cls, ref).addCallback(reactor))

    @classmethod
    def whenResolvedOnly(cls, ref, reactor):
        """Add a callback @reactor to be run when the ref is resolved.

        Discard the return value.
        """
        ref_resolution(cls, ref)

    @classmethod
    def whenBroken(cls, ref, reactor):
        """Add a callback @reactor to be run when the ref is broken.

        Return an IRef representing the result of the reactor.
        """
        return WaitingRef(ref_resolution(cls, ref).
                          addCallback(if_smashed(reactor)))

    @classmethod
    def whenBrokenOnly(cls, ref, reactor):
        """Add a callback @reactor to be run when the ref is broken.

        Discard the return value.
        """
        ref_resolution(cls, ref).addCallback(if_smashed(reactor))


class RefBase(object):
    @property
    def resolution(self):
        return self

    def resolved_handler(self):
        d = Deferred()
        self.result.addCallback(d.callback)
        return d

    def on_message(self, verb, args):
        return LocalCallRef(self, verb, args)


@zi.implementer(api.IRef)
class Broken(RefBase):
    """A Broken IRef.

    Represents the problem self.problem.
    """
    status = BROKEN

    def __init__(self, problem):
        self.problem = problem

    def on_message(self, verb, args):
        return self

    @property
    def result(self):
        d = Deferred()
        d.callback(self)
        return d

    def __repr__(self):
        return "<Broken Ref %s>" % (type(self.problem).__name__,)


@zi.implementer(api.IRef)
class Fulfilled(RefBase):
    """A Fulfilled IRef.

    Represents the value self.value.
    """
    status = FULFILLED

    def __init__(self, value):
        self.value = value

    @property
    def result(self):
        d = Deferred()
        d.callback(self)
        return d

    def __repr__(self):
        return "<Ref %r>" % (self.value,)

    def on_message(self, verb, args):
        return Ref.resolve(self.value.on_message(verb, args))


class WaitingResolver(Fulfilled):
    """A Promise Resolver.

    When its resolve method is invoked, the associated promise will be
    resolved.
    """
    def resolve(self, value):
        self.value.callback(Ref.resolve(value))

    def on_message(self, verb, args):
        if verb != "resolve":
            raise standard_attribute_error(self, verb)
        self.resolve(*args)

    def __repr__(self):
        return "<Resolver>"


@zi.implementer(api.IRef)
class LocalCallRef(RefBase):
    def __init__(self, base, verb, args):
        self.base = base
        self.verb = verb
        self.target = UNRESOLVED_SENTINEL

        resolved_handler = base.resolved_handler()
        @resolved_handler.addCallback
        def result(base):
            print base
            self.target = Ref.resolve(base.on_message(verb, args))
            return self.target
        self.result = result

    @property
    def resolution(self):
        if self.target is not UNRESOLVED_SENTINEL:
            return self.target
        return self

    @property
    def status(self):
        return self.base.status

    def __repr__(self):
        return "<Ref %s(...)>" % (self.verb,)


@zi.implementer(api.IRef)
class Never(object):
    """An IRef that can never be resolved.
    """
    status = UNRESOLVED
    def resolved_handler(self):
        return Deferred() # will never be resolved

    def on_message(self, verb, args):
        return NEVER

    def __repr__(self):
        return "<Never>"

    @property
    def resolution(self):
        return self


NEVER = Never()


@zi.implementer(api.IRef)
class FarRef(Never):
    """An IRef that represents some remote object.
    """
    status = UNRESOLVED
    def __init__(self, captp):
        self.captp = captp

    def on_message(self, verb, args):
        return self.captp.send_deliver(self, verb, args)

    def __repr__(self):
        return "<FarRef>"


@zi.implementer(api.IRef)
class WaitingRef(RefBase):
    """An IRef that will take on the value of some promise.
    """
    def __init__(self, promise):
        self.result = promise
        self.status = UNRESOLVED
        self.target = UNRESOLVED_SENTINEL

        def resolver(value):
            self.target = value
            self.status = FULFILLED
            return value

        def smasher(reason):
            self.target = Ref.broken(reason)
            self.status = BROKEN
            raise reason

        promise.addCallbacks(resolver, smasher)

    def __repr__(self):
        return "<Waiting Ref>"

    @property
    def resolution(self):
        if self.target is not UNRESOLVED_SENTINEL:
            return self.target
        return self


@zi.implementer(api.IUncallable)
class Proxy(object):
    def __init__(self, ref, uncaller):
        self._ref = ref
        self._uncaller = uncaller

    def opt_uncall(self, builder, captp):
        return self._uncaller.opt_uncall(builder, captp)

    def __getattr__(self, name):
        def proxy_method(*args):
            return self._ref.on_message(name, *args)
        return proxy

