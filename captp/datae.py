"""JSON Transport for captp conversation.

Based loosely on captp-caja's Surgeon module.
"""

from collections import Counter
from zope.interface.registry import Components
import zope.interface as zi
from twisted.internet import defer

from captp import api

class Atom(object):
    def __init__(self, value):
        self.value = value

    def visit(self, visitor):
        return visitor.visit_atom(self)


class Call(object):
    def __init__(self, promise_id, target, verb, args, recursive):
        self.promise_id = promise_id
        self.target = target
        self.verb = verb
        self.args = args
        self.recursive = recursive

    def visit(self, visitor):
        return visitor.visit_call(self)


class Import(object):
    def __init__(self, name):
        self.name = name

    def visit(self, visitor):
        return visitor.visit_import(self)


class IBID(object):
    def __init__(self, promise_id):
        self.promise_id = promise_id

    def visit(self, visitor):
        return visitor.visit_ibid(self)


class DataEBuilder(object):
    def __init__(self):
        self.usages = Counter()
        self.defined = {}

    def promise_id(self, value):
        id_key = identity_key(value)
        if id_key in self.defined:
            return self.defined[id_key], True
        promise_id = len(self.defined)
        self.defined[id_key] = promise_id
        self.usages[promise_id] += 1
        return promise_id, False

    def build_atom(self, value):
        return Atom(value)

    def build_call(self, promise_id, receiver, verb, args):
        recursive = promise_id is not None and self.usages[promise_id] != 1
        return Call(promise_id, receiver, verb, args, recursive)

    def build_import(self, name):
        return Import(name)

    def build_ibid(self, promise_id):
        self.usages[promise_id] += 1
        return IBID(promise_id)


class DataEToJSON(object):
    def __init__(self, message):
        self.defined = set()
        self.message = message

    def handle_definition(self, datae, json_value):
        promise_id = datae.promise_id
        self.defined.add(promise_id)
        if datae.recursive:
            return [u'defrec', promise_id, json_value]
        elif self.message.usages[promise_id] > 1:
            return [u'define', promise_id, json_value]
        return json_value

    def visit_call(self, call):
        if call.promise_id in self.defined:
            return [u'ibid', call.promise_id]
        call_json = [u'call',
                     call.target.visit(self),
                     call.verb,
                     [arg.visit(self) for arg in call.args]]
        return self.handle_definition(call, call_json)

    def visit_atom(self, atom):
#        if atom.promise_id in self.defined:
#            return [u'ibid', atom.promise_id]
#        return self.handle_definition(atom, atom.value)
        return atom.value

    def visit_import(self, imp):
        return [u'import', imp.name]

    def visit_ibid(self, ibid):
        return [u'ibid', ibid.promise_id]


############################################################
### Serialisation

class SubgraphKitRecogniser(object):
    def __init__(self, captp, builder):
        self.captp = captp
        self.builder = builder

    def is_atom(self, x):
        return isinstance(x, (int, unicode, float))

    def represent(self, item):
        return api.adapt(self.captp.uncallers, api.IRepresentable, item)

    def uncall(self, item):
        # TODO: should default to pass-by-proxy?
        return api.adapt(self.captp.uncallers, api.IUncallable, item)

    def uncaller(self, specimen):
        uncaller = self.uncall(specimen)
        if uncaller is None:
            raise api.CapTPException("No uncaller for %.200r" %
                                     (specimen,))
        return uncaller.opt_uncall(self, self.captp)

    def recognise(self, specimen):
        # TODO: resolve refs first.
        if self.is_atom(specimen):
            # for the moment, atoms are not cached.
            return self.builder.build_atom(specimen)

        representer = self.represent(specimen)
        if representer is not None:
            return representer.represent(self)

        if isinstance(specimen, bytes):
            raise api.CapTPException("Attempt to serialise byte string: %.200r" % (specimen,))

        promise_id, present = self.builder.promise_id(specimen)
        if present:
            return self.builder.build_ibid(promise_id)
        target, verb, args = api.opt_uncall(self.captp, specimen)
#        target, verb, args = self.uncaller(specimen)
        target, args = self.recognise(target), map(self.recognise, args)
        return self.builder.build_call(promise_id, target, verb, args)

############################################################
### Deserialisation


class SubgraphKitEvaluator(object):
    def __init__(self, initial_env):
        self.initial_environment = dict(initial_env)
        self.local_environment = {}

    def apply(self, ob):
        if isinstance(ob, unicode):
            return self.handle_atom('atom', ob)
        try:
            tag = ob[0]
        except TypeError:
            return self.handle_atom('atom', ob)
        return getattr(self, 'handle_%s' % (tag,))(*ob)

    def handle_atom(self, tag, atom):
        return atom

    def handle_import(self, tag, name):
        # we could probably support more advanced usages of import here.
        return self.initial_environment[name]

    def handle_ibid(self, tag, index):
        return self.local_environment[index]

    def handle_call(self, tag, receiver, verb, args):
        # assume this should be eager
        rec = api.adapt(self.captp.uncaller, api.IRef, self.apply(receiver))
        return rec.on_message(verb, *map(self.apply, args))

    def handle_define(self, tag, index, value):
        result = self.apply(value)
        self.local_environment[index] = result
        return result

    def handle_defrec(self, tag, promise_id, value):
        # TODO: make it a ref?
        promise = self.local_environment[promise_id] = defer.Deferred()
        result = self.apply(value)
        promise.callback(result)
        return value


def identity_key(ob):
    try:
        _hash = hash(ob)
        return ob
    except TypeError:
        return IdentityKey(id(ob), ob)


class IdentityKey(object):
    def __init__(self, id, key):
        self.id = id
        self.key = key

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        if isinstance(other, IdentityKey):
            return self.key is other.key
        return NotImplemented


# TODO: describe what is typically expected in the env.
def deserialise(representation, env):
    return SubgraphKitEvaluator(env).apply(representation)


def serialise(value, captp):
    builder = DataEBuilder()
    de = SubgraphKitRecogniser(captp, builder).recognise(value)
    return de.visit(DataEToJSON(builder))
