import zope.interface as zi
from captp.api import (IUncallable, IHaveModel, ISelfless, ISelfish,
                       WHEN_MORE_RESOLVED_MESSAGE, WHEN_BROKEN_MESSAGE)
from twisted.internet import defer

@zi.implementer(IUncallable)
class StandardUncaller(object):
    def __init__(self, context):
        self.context = context

    def opt_uncall(self, builder, captp):
        return [captp.desc_maker_standin, self.name,
                self.get_args(builder, captp)]

    def get_args(self, builder, captp):
        return self.context


class ListUncaller(StandardUncaller):
    name = u"List"


class SetUncaller(StandardUncaller):
    name = u"Set"


class TupleUncaller(StandardUncaller):
    name = u"Tuple"


class DictUncaller(StandardUncaller):
    name = u'Dict'
    def get_args(self, builder, captp):
        return [item
                for k, v in sorted(self.context.items())
                for item in (k, v)]


@zi.implementer(IUncallable)
class ByRefUncaller(object):
    def __init__(self, identity):
        self.identity = identity
        
    def opt_uncall(self, builder, captp):
        index = captp.index_for(self.identity)
        maker = captp.desc_maker_standin
        if index is not None:
            return maker, "Import", [index]
        swiss_number = captp.swiss_table.get_new(self.identity)
        return maker, u"NewFar", [captp.new_far_pos(self.identity),
                                  swiss_number.hash()]


def resolve_remote(captp, target):
    """Forward local callbacks to the remote copy.
    """
    def remote_resolve(value):
        captp.send_only(target, api.WHEN_MORE_RESOLVED_MESSAGE, value)
        return value
    def remote_smash(e):
        captp.send_only(target, api.WHEN_BROKEN_MESSAGE, e)
        raise e
    defer.Deferred(target).addCallbacks(remote_resolve, remote_smash)


@zi.implementer(IUncallable)
class PromiseUncaller(object):
    def __init__(self, target):
        self.target = target

    def opt_uncall(self, builder, captp):
        swiss_base = captp.swiss_table.next()
        answer_pos, record = captp.prepare_question(True)
#        captp.send_only(self.target, ref.WHEN_MORE_RESOLVED_MESSAGE,
#                        [record.local_reference])
        resolve_remote(captp, self.target)
        args = [captp.bind_export(self.target), answer_pos, swiss_base]
        return captp.desc_maker_standin, u"NewRemotePromise", args


def create_registry(registry=None):
    if registry is None:
        from zope.interface.registry import Components
        registry = Components()
        #from zope.interface.adapter import AdapterRegistry
        #registry = AdapterRegistry()
    def uncallable(provider, requirement):
        registry.registerAdapter(provider, [requirement], IUncallable)
        #registry.register(requirements, IUncallable, '', provider)
    uncallable(PromiseUncaller, defer.Deferred)
    uncallable(ListUncaller, list)
    uncallable(SetUncaller, set)
    uncallable(SetUncaller, frozenset)
    uncallable(TupleUncaller, tuple)
    uncallable(DictUncaller, dict)
    from captp import model_support
    uncallable(model_support.SelflessUncallable, ISelfless)
    uncallable(model_support.SelfishUncallable, ISelfish)
    return registry
