CapTP on Twisted
================

:Author: William ML Leslie <william.leslie.ttg@gmail.com>
:Date: $Date 2015-01-19 23:16:00 +1100 (Mon, 19 Jan 2015) $

This is an implementation of `the captp protocol
<http://www.erights.org/elib/distrib/captp/index.html>`_ atop Twisted
Python that aims to be binary compatible with servers written in
E-on-Java and E-on-CL.

CapTP makes building efficient, flexible, secure inter-process APIs
possible, in a way that almost resembles remote code.

When it's usable, I'll add some information about how to do so.
